#include "file.h"

enum status_fio open_read(char* name, FILE** file) {
    if(!name) return FILE_ERROR;
    *file = fopen(name, "rb");
    if(!file) return FILE_ERROR;
    return FILE_OPENED;
}

enum status_fio open_write(char* name, FILE** file) {
    if(!name) return FILE_ERROR;
    *file = fopen(name, "wb");
    if(!file) return FILE_ERROR;
    return FILE_OPENED;
}
