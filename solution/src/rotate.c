#include "rotate.h"

static inline struct pixel* rotation(struct pixel* inpparam, int width, int height) {
    int cntr = 0;
    struct pixel* finished = malloc(width * height * sizeof(struct pixel));

    for(int oldy=0; oldy<width; oldy++) {
        for(int oldx = height - 1; oldx >= 0; oldx--) {
            finished[cntr] = inpparam[oldx * width + oldy];
            cntr++;
        }
    }
    return finished;
}

struct image transformation(struct image const* inpparam) {
    struct image finished;
    if(inpparam) {
        finished.p = rotation(inpparam->p, inpparam->width, inpparam->height);
        finished.width = inpparam->height;
        finished.height = inpparam->width;
        return finished;
    }
    finished.p = NULL;
    finished.width = 0; finished.height = 0;
    return finished;
}
