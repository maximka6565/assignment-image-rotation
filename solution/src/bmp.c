#include "bmp.h"

int padding(uint32_t width, FILE* file, int stts) {
    uint8_t howmanytimes=(4 - width * sizeof(struct pixel)  % 4) % 4;
    uint8_t pad=0;
    for(uint8_t i=0; i<howmanytimes; i++) {
        if(stts == 0)
            if(fread(&pad, sizeof(uint8_t), 1, file) != 1) return 1;
        if(stts == 1)
            if(fwrite(&pad, sizeof(uint8_t), 1, file) != 1) return 1;
    }
    return 0;
}

static struct bmp_header read_bmp_header(FILE* source_file) {
    struct bmp_header header = {0};
    fread(&header, sizeof(struct bmp_header), 1, source_file);
    return header;
}

static struct pixel onepixel(FILE* source_file) {
    struct pixel current_pixel;
    fread(&current_pixel, sizeof(struct pixel), 1, source_file);
    return current_pixel;
}

enum status_r get_pxls(struct image* destination, FILE* source_file) {
    if(!destination) return READ_ERROR;

    struct bmp_header header = read_bmp_header(source_file);

    if(header.bfType != 19778) return READ_ERROR;

    uint32_t width = header.biWidth;
    uint32_t height = header.biHeight;
    uint16_t size_pixel = header.biBitCount;
    if(width == 0) return READ_ERROR;
    if(height == 0) return READ_ERROR;
    if(size_pixel != 8 * sizeof(struct pixel)) return READ_ERROR;

    destination->p = malloc(width * height * size_pixel);

    int pix_num = 0;
    for(int y=0; y<height; y++)
    {
        for(int x=0; x<width; x++)
        {
            *(destination->p + pix_num) = onepixel(source_file);
            pix_num++;
        }
        if(padding(width, source_file, 0) == 1) return READ_ERROR;
    }

    destination->width = width;
    destination->height = pix_num / width;
    return READ_OK;
}

static void bmp_header_generator(struct bmp_header* header, struct image const* image)
{
    uint32_t width = image->width, height = image->height;

    if(header)
    {
        header->bfType = 19778;
        header->biHeight = height;
        header->biWidth = width;
        header->biBitCount = sizeof(struct pixel) * 8;
    }
}

static int header_out(struct image const* source, FILE* outfile)

{
    struct bmp_header header = {
            .bfType = 0
    };
    bmp_header_generator(&header, source);
    if (fwrite(&header, sizeof(struct bmp_header), 1, outfile) != 1) return 1;
    return 0;
}

static int write_pixel(struct pixel* pixtowrite, FILE* outfile) {
    if (fwrite(pixtowrite, sizeof(struct pixel), 1, outfile) != 1) return 1;
    return 0;
}

enum status_w write_bmp(struct image const* source, FILE* outfile) {
    uint32_t pix_num=0;
    if(source == NULL) return WRITE_ERROR;
    uint32_t width = source->width, height = source->height;
    if (header_out(source, outfile) == 1) return WRITE_ERROR;
    for(uint32_t y=0; y<height; y++) {
        for(uint32_t x=0; x<width; x++) {
            if(write_pixel(source->p + pix_num, outfile) == 1) return WRITE_ERROR;
            pix_num++;
        }
        if(padding(width, outfile, 1) == 1) return WRITE_ERROR;
    }
    return WRITE_OK;
}
