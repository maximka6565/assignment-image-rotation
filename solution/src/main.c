#include <stdio.h>
#include <string.h>

#include "bmp.h"
#include "file.h"
#include "image.h"
#include "rotate.h"

static void free_image(struct image img) {
    free(img.p);
}

int main( int argc, char** argv ) {
    (void) argc; (void) argv;

    FILE* inp = NULL;
    if(open_read(argv[1], &inp) == READ_ERROR) {
        fprintf(stderr, "Error in open first file");
        return 1;
    }
    FILE* outp = NULL;
    if(open_write(argv[2], &outp) == WRITE_ERROR) {
        fprintf(stderr, "Error in open second file");
        return 1;
    }
    
    struct image inppixls;
    enum status_r sr = get_pxls(&inppixls, inp);
    if(sr == READ_ERROR) return 1;
    struct image transf = transformation(&inppixls);
    enum status_w sw =  write_bmp(&(transf), outp);
    if(sw == WRITE_ERROR) {
        free_image(inppixls);
        return 1;
    }
    
    if(fflush(outp) != 0)  {
        fprintf(stderr, "Error in writing file");
        return 1;
    }
    if(fclose(outp) != 0 || fclose(inp) != 0) {
        fprintf(stderr, "Error in closing file");
        return 1;
    }
    
    free_image(transf);
    free_image(inppixls);
    return 0;
}
