#ifndef ASSIGNMENT_IMAGE_ROTATION_FILE_H
#define ASSIGNMENT_IMAGE_ROTATION_FILE_H

#include <stdio.h>
#include <stdlib.h>

enum status_fio {
    FILE_OPENED,
    FILE_ERROR
};

enum status_fio open_read(char* name, FILE** file);

enum status_fio open_write(char* name, FILE** file);
#endif //ASSIGNMENT_IMAGE_ROTATION_FILE_H

