#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include "image.h"

#include <inttypes.h>
#include  <stdint.h>
#include <stdio.h>
#include <malloc.h>

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

enum status_r{
    READ_OK,
    READ_ERROR
};

enum status_w {
    WRITE_OK,
    WRITE_ERROR
};

int padding(uint32_t width, FILE* file, int stts);

enum status_r get_pxls(struct image* destination, FILE* source_file);

enum status_w write_bmp(struct image const* source, FILE* outfile);



#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H

