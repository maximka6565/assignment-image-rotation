#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include  <stdint.h>

struct pixel {
    uint8_t r, g, b;
};

struct image {
    struct pixel* p;
    uint64_t width;
    uint64_t height;
};

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

